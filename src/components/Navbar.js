import React, { Component } from 'react';
import { Grid, Menu, Icon, Header } from 'semantic-ui-react';

class Navbar extends Component {
  render() {
    return (
      <div>
        <Grid columns={1}>
          <Grid.Column>
            <Menu inverted widths={1}>
              <Menu.Item>
                <Header inverted as="h2">
                  <Icon name="line graph" />
                  <Header.Content>Plot Equations</Header.Content>
                </Header>
              </Menu.Item>
            </Menu>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default Navbar;

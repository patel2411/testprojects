import React, { Component } from 'react';
import d3 from 'd3';
import functionPlot from 'function-plot';
import { Container, Button, Input, Grid, Label } from 'semantic-ui-react';
import MessageModal from './MessageModal';
import Sidebar from './Sidebar';
import HistoryModal from './HistoryModal';

class Plot extends Component {
  constructor() {
    super();

    let lastEquation = localStorage.getItem('lastEquation');
    lastEquation = lastEquation ? JSON.parse(lastEquation) : 'x^2';

    this.state = {
      isError: false,
      errorMessage: '',
      xDomainLower: -10,
      xDomainUpper: 10,
      yDomainLower: -10,
      yDomainUpper: 10,
      equation: lastEquation,
      xle: false,
      xue: false,
      yle: false,
      yue: false,
      device: 'other'
    };
  }

  addToStorage = equation => {
    let pastEquations = localStorage.getItem('pastEquations');
    pastEquations = pastEquations ? JSON.parse(pastEquations) : [];
    if (pastEquations.indexOf(equation) < 0) pastEquations.push(equation);
    localStorage.setItem('pastEquations', JSON.stringify(pastEquations));
    localStorage.setItem('lastEquation', JSON.stringify(equation));
  };

  plotEquation = equation => {
    const plot = document.getElementById('plot');
    try {
      functionPlot({
        target: plot,
        height: this.state.graphHeight,
        width: this.state.graphWidth,
        xAxis: {
          domain: [this.state.xDomainLower, this.state.xDomainUpper],
          label: 'x-axis'
        },
        yAxis: {
          domain: [this.state.yDomainLower, this.state.yDomainUpper],
          label: 'y-axis'
        },
        tip: {
          renderer: function() {}
        },
        grid: true,
        data: [
          {
            fn: equation
          }
        ]
      });

      this.addToStorage(equation);
      // this.setState({ ...this.state });
    } catch (e) {
      console.log(e);
      this.setState({
        isError: true,
        errorMessage: 'Invalid expression!'
      });
    }
  };

  validate = () => {
    let xl = this.state.xDomainLower;
    let xu = this.state.xDomainUpper;
    let yl = this.state.yDomainLower;
    let yu = this.state.yDomainUpper;
    let valid = true;
    if (isNaN(parseInt(xl))) {
      this.setState({
        xle: true
      });
      valid = false;
    } else
      this.setState({
        xle: false
      });

    if (isNaN(parseInt(xu))) {
      this.setState({
        xue: true
      });
      valid = false;
    } else
      this.setState({
        xue: false
      });

    if (isNaN(parseInt(yl))) {
      this.setState({
        yle: true
      });
      valid = false;
    } else
      this.setState({
        yle: false
      });

    if (isNaN(parseInt(yu))) {
      this.setState({
        yue: true
      });
      valid = false;
    } else
      this.setState({
        yue: false
      });

    if (xl >= xu) {
      this.setState({
        isError: true,
        errorMessage: 'x-lower cannot be grater than or equal to x-larger',
        xle: true,
        xue: true
      });
      valid = false;
    }

    if (yl >= yu) {
      this.setState({
        isError: true,
        errorMessage: 'y-lower cannot be grater than or equal to y-larger',
        yle: true,
        yue: true
      });
      valid = false;
    }
    return valid;
  };

  handlePlotButton = e => {
    let isValid = this.validate();
    if (isValid) this.plotEquation(this.state.equation);
  };

  handleDomainChange = e => {
    const name = e.target.name;
    const value = e.target.value;

    if (name === 'xDomainLower')
      this.setState({
        xDomainLower: value,
        xle: false
      });
    if (name === 'xDomainUpper')
      this.setState({
        xDomainUpper: value,
        xue: false
      });
    if (name === 'yDomainLower')
      this.setState({
        yDomainLower: value,
        yle: false
      });
    if (name === 'yDomainUpper')
      this.setState({
        yDomainUpper: value,
        yue: false
      });
  };

  handleEquationChange = e => {
    this.setState({ equation: e.target.value });
  };

  handleHistoryButton = () => {
    this.setState({
      openHistory: true
    });
  };

  handleModalClose = () => {
    this.setState({
      isError: false,
      openHistory: false,
      errorMessage: ''
    });
  };

  plotHandler = eq => {
    this.setState({ equation: eq });
    this.plotEquation(eq);
  };

  updateDimensions = () => {
    let deviceWidth = window.innerWidth;
    let deviceHeight = window.innerHeight;

    this.setState({
      graphHeight: 0.6 * deviceHeight,
      graphWidth: 0.6 * deviceWidth
    });

    if (deviceWidth < 768) {
      this.setState({
        graphHeight: 0.6 * deviceHeight,
        graphWidth: 0.75 * deviceWidth,
        device: 'tablet'
      });
    } else {
      this.setState({
        graphHeight: 0.6 * deviceHeight,
        graphWidth: 0.85 * deviceHeight,
        device: 'other'
      });
    }

    this.plotEquation(this.state.equation);
  };

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.plotEquation(this.state.equation);
  }

  render() {
    return (
      <div>
        <MessageModal
          modalOpen={this.state.isError}
          errorMessage={this.state.errorMessage}
          closeModal={this.handleModalClose}
        />
        <HistoryModal
          modalOpen={this.state.openHistory}
          closeModal={this.handleModalClose}
          updateHistory={this.handlePlotButton}
          updatePlot={this.plotHandler}
          updateEquation={this.handleEquationChange}
        />
        <Container fluid>
          <Grid textAlign="center" padded stackable columns={2}>
            <Grid.Column width={12}>
              <Grid.Row>
                <div id="plot" />
              </Grid.Row>
              <Grid.Row>
                <Grid textAlign="center" padded columns={1}>
                  <Grid.Column>
                    <Input
                      type="text"
                      labelPosition="right"
                      placeholder="Equation..."
                      size="large"
                      action
                    >
                      <Label>y(x)</Label>
                      <input
                        value={this.state.equation}
                        onChange={this.handleEquationChange}
                      />
                      <Button
                        primary
                        type="submit"
                        onClick={this.handlePlotButton}
                      >
                        Plot
                      </Button>
                    </Input>
                  </Grid.Column>
                </Grid>
                <Grid stackable padded columns={2}>
                  <Grid.Column
                    textAlign={
                      this.state.device === 'tablet' ? 'center' : 'right'
                    }
                  >
                    <Input
                      label="x-axis lower"
                      placeholder="-10"
                      name="xDomainLower"
                      value={this.state.xDomainLower}
                      onChange={this.handleDomainChange}
                      error={this.state.xle}
                    />
                  </Grid.Column>
                  <Grid.Column
                    textAlign={
                      this.state.device === 'tablet' ? 'center' : 'left'
                    }
                  >
                    <Input
                      label="x-axis upper"
                      placeholder="10"
                      name="xDomainUpper"
                      value={this.state.xDomainUpper}
                      onChange={this.handleDomainChange}
                      error={this.state.xue}
                    />
                  </Grid.Column>
                </Grid>
                <Grid stackable padded columns={2}>
                  <Grid.Column
                    textAlign={
                      this.state.device === 'tablet' ? 'center' : 'right'
                    }
                  >
                    <Input
                      label="y-axis lower"
                      placeholder="-10"
                      name="yDomainLower"
                      value={this.state.yDomainLower}
                      onChange={this.handleDomainChange}
                      error={this.state.yle}
                    />
                  </Grid.Column>
                  <Grid.Column
                    textAlign={
                      this.state.device === 'tablet' ? 'center' : 'left'
                    }
                  >
                    <Input
                      label="y-axis upper"
                      placeholder="10"
                      name="yDomainUpper"
                      value={this.state.yDomainUpper}
                      onChange={this.handleDomainChange}
                      error={this.state.yue}
                    />
                  </Grid.Column>
                </Grid>
              </Grid.Row>
              {this.state.device === 'tablet' && (
                <Grid.Row centered>
                  <Button
                    type="submit"
                    onClick={this.handleHistoryButton}
                    color="black"
                  >
                    View History
                  </Button>
                </Grid.Row>
              )}
            </Grid.Column>
            {this.state.device === 'other' && (
              <Grid.Column width={4}>
                <Sidebar
                  updateHistory={this.handlePlotButton}
                  updatePlot={this.plotHandler}
                  updateEquation={this.handleEquationChange}
                />
              </Grid.Column>
            )}
          </Grid>
        </Container>
      </div>
    );
  }
}

export default Plot;

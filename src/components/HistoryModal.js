import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Header, Icon, Modal, Segment } from 'semantic-ui-react';

class HistoryModal extends Component {
  handleClose = () => {
    this.props.closeModal();
  };

  handleClearButton = () => {
    localStorage.clear();
    this.props.updateHistory();
  };

  handlePlotButton = e => {
    this.props.updatePlot(e.target.value);
    this.props.updateEquation(e);
    this.handleClose();
  };

  render() {
    const getHistory = () => {
      let pastEquations = localStorage.getItem('pastEquations');
      pastEquations = pastEquations ? JSON.parse(pastEquations) : [];
      return pastEquations;
    };
    return (
      <div>
        <Modal
          open={this.props.modalOpen}
          onClose={this.handleClose}
          basic
          size="small"
        >
          <Modal.Content>
            <Segment.Group>
              <Segment textAlign="center" secondary>
                <Header icon>
                  <Icon name="history" />
                  Equations plotted so far
                </Header>
                <Segment.Inline>
                  <Button
                    type="submit"
                    onClick={this.handleClearButton}
                    color="red"
                  >
                    Clear History
                  </Button>
                </Segment.Inline>
              </Segment>

              {getHistory().map((item, index) => (
                <Segment key={index} clearing>
                  <Header floated="left">{item}</Header>
                  <Button
                    floated="right"
                    size="small"
                    type="submit"
                    value={item}
                    onClick={this.handlePlotButton}
                    positive
                  >
                    Plot
                  </Button>
                </Segment>
              ))}
            </Segment.Group>
          </Modal.Content>
          <Modal.Actions>
            <Button color="grey" onClick={this.handleClose} inverted>
              <Icon name="close" /> Close
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

HistoryModal.propTypes = {
  updatePlot: PropTypes.func,
  updateHistory: PropTypes.func,
  updateEquation: PropTypes.func
};

export default HistoryModal;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

class Equations extends Component {
  handlePlotButton = () => {
    this.plotEquation(this.state.equation);
  };

  render() {
    return (
      <div>
        <Table.Row>
          <Table.Cell onMouseEnter={this.showPlotButton}>
            {this.props.eq}
          </Table.Cell>
        </Table.Row>
      </div>
    );
  }
}

Equations.propTypes = {
  updatePlot: PropTypes.func
};

export default Equations;

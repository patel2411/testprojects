import React, { Component } from 'react';
import Navbar from './Navbar';
import Plot from './Plot';

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Plot />
      </div>
    );
  }
}

export default App;

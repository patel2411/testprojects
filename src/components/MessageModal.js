import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

class MessageModal extends Component {
  handleClose = () => {
    this.props.closeModal();
  };
  render() {
    return (
      <div>
        <Modal
          open={this.props.modalOpen}
          onClose={this.handleClose}
          basic
          size="small"
        >
          <Header icon="meh" content="Something went wrong!" />
          <Modal.Content>
            <h3>{this.props.errorMessage}</h3>
          </Modal.Content>
          <Modal.Actions>
            <Button color="grey" onClick={this.handleClose} inverted>
              <Icon name="close" /> Close
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

export default MessageModal;

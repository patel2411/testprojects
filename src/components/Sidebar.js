import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Segment, Header, Icon } from 'semantic-ui-react';

class Sidebar extends Component {
  handleClearButton = () => {
    localStorage.clear();
    this.props.updateHistory();
  };

  handlePlotButton = e => {
    this.props.updatePlot(e.target.value);
    this.props.updateEquation(e);
  };

  render() {
    const getHistory = () => {
      let pastEquations = localStorage.getItem('pastEquations');
      pastEquations = pastEquations ? JSON.parse(pastEquations) : [];
      return pastEquations;
    };

    return (
      <div>
        <Segment.Group style={{ overflow: 'auto', maxHeight: '27em' }}>
          <Segment textAlign="center" secondary>
            <Header icon>
              <Icon name="history" />
              Equations plotted so far
            </Header>
            <Segment.Inline>
              <Button
                type="submit"
                onClick={this.handleClearButton}
                color="red"
              >
                Clear History
              </Button>
            </Segment.Inline>
          </Segment>
          {getHistory().map((item, index) => (
            <Segment key={index} clearing>
              <Header floated="left">{item}</Header>
              <Button
                floated="right"
                size="mini"
                type="submit"
                value={item}
                onClick={this.handlePlotButton}
                positive
              >
                Plot
              </Button>
            </Segment>
          ))}
        </Segment.Group>
      </div>
    );
  }
}

Sidebar.propTypes = {
  updatePlot: PropTypes.func,
  updateHistory: PropTypes.func,
  updateEquation: PropTypes.func
};

export default Sidebar;
